# Issues in Pre-Home

### IE

1 - password field - enabled/disable see password button (native button from IE)

2 - number input field don't show correctly the loader animation effect

3 - font don't render

4 - Modal "Termos e ..." don't animate appropriate (don't sladeIn/Out)


### Firefox

1 - 'Positiva��o in�cio' animation left don't work


### Safari

1 - Input field outline showing

2 - in 'Pre-home' screen, component .sidebar-prehome show about. Fix: change display to block and float: left;



### Chrome, Firefox, IE

1 - transition on enter new rote inside the 'Pre-Home'

